import { FunctionComponent, PropsWithChildren } from "react";
import NextHead from "next/head";

const Head: FunctionComponent<PropsWithChildren<{}>> = (props) => {
    return <NextHead children={props.children} />;
};

export default Head;
