import { FunctionComponent } from "react";
import { Category, Note } from "../../common/interfaces";
import CategoryView from "./CategoryView";

interface CategoryListProps {
    parentCategory?: Category;
    categories: Category[];
    notes: Note[];
}

const CategoryList: FunctionComponent<CategoryListProps> = (props) => {
    const rootCategories =
        props.parentCategory === undefined
            ? props.categories.filter((c) => c.parentNoteCategoryId === null)
            : props.categories.filter(
                  (c) => c.parentNoteCategoryId === props.parentCategory?.id
              );

    return (
        <div>
            {rootCategories.map((c) => (
                <CategoryView
                    key={c.id}
                    category={c}
                    subCategories={props.categories.filter(
                        (sc) => sc.parentNoteCategoryId === c.id
                    )}
                    notes={props.notes}
                />
            ))}
        </div>
    );
};

export default CategoryList;
