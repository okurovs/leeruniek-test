import { FunctionComponent, useState } from "react";
import { Category, Note } from "../../common/interfaces";
import NoteList from "../note/NoteList";
import CategoryList from "./CategoryList";
import styles from "./CategoryView.module.css";

interface CategoryListProps {
    category: Category;
    subCategories: Category[];
    notes: Note[];
}

const CategoryView: FunctionComponent<CategoryListProps> = (props) => {
    const [isExpanded, setIsExpanded] = useState(true);
    const arrowStyles = [styles.arrow];
    if (isExpanded) {
        arrowStyles.push(styles.active);
    }

    const categoryNotes = props.notes.filter(
        (n) =>
            n.categoryId === props.category.id &&
            n.groupPlanId === props.category.groupPlanId
    );

    return (
        <div
            className={styles.CategoryView}
            data-testid={"category-" + props.category.id}
        >
            <button
                className={styles.headingButton}
                type="button"
                onClick={() => setIsExpanded(!isExpanded)}
            >
                <h4>{props.category.name}</h4>
                <div className={arrowStyles.join(" ")}></div>
            </button>
            {isExpanded && (
                <div className={styles.expandedView}>
                    {categoryNotes.length > 0 && (
                        <NoteList notes={categoryNotes} />
                    )}
                    <CategoryList
                        parentCategory={props.category}
                        categories={props.subCategories}
                        notes={props.notes}
                    />
                </div>
            )}
        </div>
    );
};

export default CategoryView;
