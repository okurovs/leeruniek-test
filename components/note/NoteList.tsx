import { FunctionComponent } from "react";
import { Note } from "../../common/interfaces";
import styles from "./NoteList.module.css";

interface NoteListProps {
    notes: Note[];
}

const NoteList: FunctionComponent<NoteListProps> = (props) => {
    props.notes.sort(
        (a, b) => Date.parse(a.dateCreated) - Date.parse(b.dateCreated)
    );

    return (
        <div className={styles.NoteList}>
            {props.notes.map((n) => (
                <div key={n.id}>
                    <p>{n.content}</p>
                    <span className={styles.date}>{n.dateCreated}</span>
                </div>
            ))}
        </div>
    );
};

export default NoteList;
