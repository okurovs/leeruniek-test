import { FunctionComponent } from "react";
import { Category, Plan, Note } from "../../common/interfaces";
import CategoryList from "../category/CategoryList";
import PlanInfo from "./PlanInfo";
import styles from "./PlanView.module.css";

interface PlanViewProps {
    plan: Plan;
    categories: Category[];
    notes: Note[];
}

const PlanView: FunctionComponent<PlanViewProps> = (props) => {
    const filteredCategories = props.categories
        .filter((c) => !c.isArchived && c.groupPlanId === props.plan.id)
        .sort((a, b) => a.name.localeCompare(b.name));

    return (
        <div className={styles.PlanView}>
            <PlanInfo plan={props.plan} />
            <CategoryList categories={filteredCategories} notes={props.notes} />
        </div>
    );
};

export default PlanView;
