import { FunctionComponent } from "react";
import { Plan } from "../../common/interfaces";
import styles from "./PlanInfo.module.css";

interface PlanInfoProps {
    plan: Plan;
}

const PlanInfo: FunctionComponent<PlanInfoProps> = (props) => {
    return (
        <div className={styles.PlanInfo}>
            <h2>{props.plan.name}</h2>
            <span>{props.plan.userCreated}</span>
        </div>
    );
};

export default PlanInfo;
