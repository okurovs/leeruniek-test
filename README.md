Leeruniek assessment solution.

## To launch the project

First, install dependencies:

```bash
yarn
```

Then, start the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) in your browser to see the result.

To run tests:

```bash
yarn test
```
