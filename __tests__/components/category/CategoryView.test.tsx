import CategoryView from "../../../components/category/CategoryView";
import { render, screen } from "@testing-library/react";
import {
    fakeNotes,
    fakeCategories,
    fakeValidNotes,
    fakeWrongPlanNote,
    fakeWrongCategoryNote,
    fakeSubcategory,
    fakeRootCategories,
} from "../../../common/testing-data";

describe("CategoryView", () => {
    beforeEach(() => {
        render(
            <CategoryView
                category={fakeRootCategories[0]}
                subCategories={[fakeSubcategory]}
                notes={fakeNotes}
            />
        );
    });

    it("renders valid notes", () => {
        for (const i in fakeValidNotes) {
            const n = fakeValidNotes[i];
            const note = screen.queryByText(n.content);
            expect(note).toBeInTheDocument();
        }
    });

    it("does not render notes from different category", () => {
        const note = screen.queryByText(fakeWrongCategoryNote.content);
        expect(note).toBeNull();
    });

    it("does not render notes from different plan", () => {
        const note = screen.queryByText(fakeWrongPlanNote.content);
        expect(note).toBeNull();
    });

    it("renders subcategories", () => {
        const category = screen.queryByText(fakeSubcategory.name);
        expect(category).toBeInTheDocument();
    });
});
