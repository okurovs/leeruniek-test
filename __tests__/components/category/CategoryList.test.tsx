import CategoryList from "../../../components/category/CategoryList";
import { render, screen } from "@testing-library/react";
import {
    fakeCategories,
    fakeSubcategory,
    fakeNotes,
} from "../../../common/testing-data";

describe("CategoryList", () => {
    beforeEach(() => {
        render(<CategoryList categories={fakeCategories} notes={fakeNotes} />);
    });

    it("renders all categories", () => {
        for (const i in fakeCategories) {
            const categoryName = screen.getByText(fakeCategories[i].name);
            expect(categoryName).toBeInTheDocument();
        }
    });

    it("renders subcategories correctly", () => {
        const rootCategory = screen.getByTestId(
            "category-" + fakeSubcategory.parentNoteCategoryId
        );
        expect(rootCategory).toHaveTextContent(fakeSubcategory.name);
    });
});
