import { render, screen } from "@testing-library/react";
import {
    fakePlan,
    fakeCategories,
    fakeArchivedCategory,
    fakeWrongPlanCategory,
    fakeRootCategories,
    fakeSubcategory,
    fakeNotes,
} from "../../../common/testing-data";
import PlanView from "../../../components/plan/PlanView";

describe("PlanView", () => {
    beforeEach(() => {
        render(
            <PlanView
                plan={fakePlan}
                categories={fakeCategories}
                notes={fakeNotes}
            />
        );
    });

    it("renders valid categories", () => {
        for (const i in fakeRootCategories) {
            const categoryName = screen.getByText(fakeRootCategories[i].name);
            expect(categoryName).toBeInTheDocument();
        }

        const categoryName = screen.queryByText(fakeSubcategory.name);
        expect(categoryName).toBeInTheDocument();
    });

    it("does not render archived categories", () => {
        const categoryName = screen.queryByText(fakeArchivedCategory.name);
        expect(categoryName).toBeNull();
    });

    it("does not render categories from different plan", () => {
        const categoryName = screen.queryByText(fakeWrongPlanCategory.name);
        expect(categoryName).toBeNull();
    });
});
