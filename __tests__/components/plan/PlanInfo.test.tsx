import PlanInfo from "../../../components/plan/PlanInfo";
import { render, screen } from "@testing-library/react";
import { fakePlan } from "../../../common/testing-data";

describe("PlanInfo", () => {
    beforeEach(() => {
        render(<PlanInfo plan={fakePlan} />);
    });

    it("renders plan name as heading", () => {
        const heading = screen.getByRole("heading");
        expect(heading).toHaveTextContent(fakePlan.name);
    });

    it("renders plan author name", () => {
        const authorName = screen.getByText(fakePlan.userCreated);
        expect(authorName).toBeInTheDocument();
    });
});
