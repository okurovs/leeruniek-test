import NoteList from "../../../components/note/NoteList";
import { render, screen } from "@testing-library/react";
import { fakeNotes } from "../../../common/testing-data";

describe("NoteList", () => {
    beforeEach(() => {
        render(<NoteList notes={fakeNotes} />);
    });

    it("renders all notes", () => {
        for (const i in fakeNotes) {
            const noteName = screen.getByText(fakeNotes[i].content);
            expect(noteName).toBeInTheDocument();
        }
    });
});
