import { Category, Note, Plan } from "../common/interfaces";

export const fakePlan: Plan = {
    id: 1,
    name: "Test Plan",
    userCreated: "Test Plan Author",
};

export const fakeRootCategories: Category[] = [
    {
        id: 1,
        isArchived: false,
        name: "Test Category 1",
        parentNoteCategoryId: null,
        groupPlanId: 1,
    },
    {
        id: 2,
        isArchived: false,
        name: "Test Category 2",
        parentNoteCategoryId: null,
        groupPlanId: 1,
    },
];

export const fakeSubcategory: Category = {
    id: 3,
    isArchived: false,
    name: "Subcategory",
    parentNoteCategoryId: 1,
    groupPlanId: 1,
};

export const fakeGoodCategories: Category[] = [
    ...fakeRootCategories,
    fakeSubcategory,
];

export const fakeArchivedCategory: Category = {
    id: 4,
    isArchived: true,
    name: "Archived Category",
    parentNoteCategoryId: null,
    groupPlanId: 1,
};

export const fakeWrongPlanCategory: Category = {
    id: 5,
    isArchived: false,
    name: "Wrong Plan Category",
    parentNoteCategoryId: 1,
    groupPlanId: 2,
};

export const fakeCategories: Category[] = [
    ...fakeGoodCategories,
    fakeArchivedCategory,
    fakeWrongPlanCategory,
];

export const fakeValidNotes: Note[] = [
    {
        id: 1,
        categoryId: 1,
        content: "Note 1",
        dateCreated: "2021-10-10",
        groupPlanId: 1,
    },
    {
        id: 2,
        categoryId: 1,
        content: "Note 2",
        dateCreated: "2021-10-11",
        groupPlanId: 1,
    },
];

export const fakeWrongCategoryNote: Note = {
    id: 3,
    categoryId: 2,
    content: "Wrong Category Note",
    dateCreated: "2021-10-11",
    groupPlanId: 1,
};

export const fakeWrongPlanNote: Note = {
    id: 4,
    categoryId: 1,
    content: "Wrong Plan Note",
    dateCreated: "2021-10-11",
    groupPlanId: 2,
};

export const fakeNotes: Note[] = [
    ...fakeValidNotes,
    fakeWrongCategoryNote,
    fakeWrongPlanNote,
];
