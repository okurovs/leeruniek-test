export interface Plan {
    id: number;
    name: string;
    userCreated: string;
}

export interface Category {
    id: number;
    isArchived: boolean;
    name: string;
    parentNoteCategoryId: number | null;
    groupPlanId: number;
}

export interface Note {
    id: number;
    categoryId: number;
    content: string;
    dateCreated: string;
    groupPlanId: number;
}
