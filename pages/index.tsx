import type { GetServerSideProps, NextPage } from "next";
import { CATEGORIES, NOTES, PLAN } from "../common/data";
import { Category, Plan, Note } from "../common/interfaces";
import Head from "../components/Head";
import PlanView from "../components/plan/PlanView";

interface HomePageProps {
    plan: Plan;
    categories: Category[];
    notes: Note[];
}

export const getServerSideProps: GetServerSideProps = async () => {
    return {
        props: {
            plan: PLAN,
            categories: CATEGORIES,
            notes: NOTES,
        },
    };
};

const HomePage: NextPage<HomePageProps> = (props) => {
    return (
        <div>
            <Head>
                <title>{props.plan.name} | Leeruniek assessment</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <PlanView
                plan={props.plan}
                categories={props.categories}
                notes={props.notes}
            />
        </div>
    );
};

export default HomePage;
